<?php

namespace App\Services;

use App\User;
use Illuminate\Support\Facades\Hash;

class AuthService 
{
    public function __construct()
    {
        $this->users = new User();
    }

    public function register($request)
    {
        $user = $this->users->register($request);

        return response()->json(['user' => $user ], 200);
    }

    public function login($request)
    {
        $user = User::where('username', $request->username)->first();
        
        if(!$user){
            return response()->json([
                'status' => 'error', 
                'message' => 'Username or passowrd is invalid!' 
            ], 401);
        }

        if(Hash::check($request->password, $user->password)){
            $user->update(['api_token' => str_random(20)]);
            return response()->json([
                'status' => 'success', 
                'message' => $user
            ], 200);
        }

        return response()->json([
            'status' => 'error', 
            'message' => 'Username or passowrd is invalid!11' 
        ], 401);
    }

    public function logout($request)
    {
        $api_token = $request->api_token;

        $user = User::where('api_token', $api_token)->first();
        
        if(!$user){
            return response()->json([
                'status' => 'error', 
                'message' => 'Something went wrong!' 
            ], 404);    
        }

        $user->api_token = null;
        $user->save();


        return response()->json([
            'status' => 'success', 
            'message' => 'You are logged out' 
        ], 401);
    }

}
