<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Services\AuthService;

class AuthController extends Controller
{

    public function __construct()
    {
        $this->AuthService = new AuthService();
    }

    public function register(Request $request)
    {
        return $this->AuthService->register($request);
    }

    public function login(Request $request)
    {
        return $this->AuthService->login($request);
    }

    public function logout(Request $request)
    {
        return $this->AuthService->logout($request);
    }

}