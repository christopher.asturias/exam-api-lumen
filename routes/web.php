<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/user/login', 'AuthController@login');
$router->get('/token/destroy', 'AuthController@logout');
$router->post('/user/register', 'AuthController@register');

$router->get('/user/search', 'UsersController@index');
// $router->get('/users', ['middeware' => 'auth', 'UsersController@index']);
// $router->get('/users/{userId}', 'UsersController@show');
// $router->post('/users', 'UsersController@store');
